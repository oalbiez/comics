# -*- coding: utf-8 -*-
from comics.model.metadata import Genre, Metadata, Title


def test_genres_should_return_empty_when_no_genre_in_metadata() -> None:
    assert Metadata.on(Title("bla")).genres() == []


def test_genres_should_return_defined_genres_in_metadata() -> None:
    assert Metadata.on(Genre("A"), Title("bla"), Genre("B")).genres() == [
        Genre("A"),
        Genre("B"),
    ]
