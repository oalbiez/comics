# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Callable
from typing import TypeVar

T = TypeVar("T")
Transformer = Callable[[T], T]


def transform(subject: T, *actions: Transformer) -> T:
    result = subject
    for action in actions:
        result = action(result)
    return result


def compose(*actions: Transformer) -> Transformer:
    def _transform(subject: T) -> T:
        return transform(subject, *actions)

    return _transform


U = TypeVar("U")
V = TypeVar("V")
W = TypeVar("W")


def comp(first: Callable[[U], V], second: Callable[[V], W]) -> Callable[[U], W]:
    def _func(value: U) -> W:
        return second(first(value))

    return _func


def strip(value: str) -> str:
    return value.strip()


def length(value: str) -> int:
    return len(value)


def sqr(value: int) -> int:
    return value * value


# ok = comp(comp(strip, length), sqr)
