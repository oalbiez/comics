# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Callable
from typing import TypeVar

Value = TypeVar("Value")
Predicate = Callable[[Value], bool]


def all_true(*predicates: Predicate) -> Predicate:
    def predicate(item: Value) -> bool:
        for predicate in predicates:
            if not predicate(item):
                return False
        return True

    return predicate


def one_true(*predicates: Predicate) -> Predicate:
    def predicate(item: Value) -> bool:
        for predicate in predicates:
            if predicate(item):
                return True
        return False

    return predicate
