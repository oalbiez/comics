
Best to work with an example: Batman Volume 2 (DC Comics):

    Number - the issue number; its String because some issues are not numbered as an Integer; see Batman 23.1

    Count - total number of issues in this volume; for Batman example its "57"

    Volume - the volume of the series; Batman example is "2" or "2011" (since there was a previous run in 1940 and followed in 2016)


# Metadatas

language: Language
publisher: Publisher

critical_rating: CriticalRating
country: Country

alternate_series: AlternateSeries
alternate_number: AlternateNumber
alternate_count: AlternateCount
imprint: Imprint
notes: Notes
web_link: WebLink
format: Format
manga: Manga
black_and_white: BlackAndWhite
page_count: PageCount
maturity_rating: MaturityRating

story_arc: StoryArc
series_group: SeriesGroup
scan_info: ScanInfo

price: Price
is_version_of: IsVersionOf
rights: Rights
identifier: Identifier
last_mark: LastMark
cover_image: CoverImage
