# -*- coding: utf-8 -*-
from __future__ import annotations

from ._credits import Credit, Person, Role
from ._metadata import (
    Characters,
    Data,
    Genre,
    Issue,
    IssueCount,
    Locations,
    MainCharacterOrTeam,
    Metadata,
    PublicationDate,
    Publisher,
    Series,
    Summary,
    Tag,
    Teams,
    Title,
    Volume,
    VolumeCount,
)
