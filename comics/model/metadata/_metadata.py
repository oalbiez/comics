# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass, field

from ._credits import Credit
from ._pages import Page


@dataclass(frozen=True)
class Characters:
    value: str


@dataclass(frozen=True)
class Genre:
    value: str


@dataclass(frozen=True)
class Issue:
    value: str


@dataclass(frozen=True)
class IssueCount:
    value: int


@dataclass(frozen=True)
class Locations:
    value: str


@dataclass(frozen=True)
class MainCharacterOrTeam:
    value: str


@dataclass(frozen=True)
class PublicationDate:
    year: int | None = None
    month: int | None = None
    day: int | None = None


@dataclass(frozen=True)
class Publisher:
    value: str


@dataclass(frozen=True)
class Series:
    name: str


@dataclass(frozen=True)
class Summary:
    value: str


@dataclass(frozen=True)
class Tag:
    value: str


@dataclass(frozen=True)
class Teams:
    value: str


@dataclass(frozen=True)
class Title:
    value: str


@dataclass(frozen=True)
class Volume:
    value: int


@dataclass(frozen=True)
class VolumeCount:
    value: int


Data = (
    Characters
    | Credit
    | Genre
    | Issue
    | IssueCount
    | Locations
    | MainCharacterOrTeam
    | Page
    | PublicationDate
    | Publisher
    | Series
    | Summary
    | Tag
    | Teams
    | Title
    | Volume
    | VolumeCount
)


@dataclass(frozen=True)
class Metadata:
    data: list[Data] = field(default_factory=list)

    @staticmethod
    def empty() -> Metadata:
        return Metadata()

    @staticmethod
    def on(*data: Data) -> Metadata:
        return Metadata(list(data))

    def genres(self) -> list[Genre]:
        return [data for data in self.data if isinstance(data, Genre)]
