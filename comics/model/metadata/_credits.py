# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from typing import NewType

Person = NewType("Person", str)
Role = NewType("Role", str)


COLORIST = Role("Colorist")
COVER_ARTIST = Role("CoverArtist")
DESIGN = Role("Design")
EDITOR = Role("Editor")
FOREWORD = Role("Foreword")
INKER = Role("Inker")
LETTERER = Role("Letterer")
OTHER = Role("Other")
PENCILLER = Role("Penciller")
SCENERY = Role("Scenery")
STORYBOARD = Role("Storyboard")
TRANSLATOR = Role("Translator")
WRITER = Role("Writer")


@dataclass(frozen=True)
class Credit:
    person: Person
    role: Role
    is_primary: bool = False

    @staticmethod
    def colorist(person: Person) -> Credit:
        return Credit(person, COLORIST)

    @staticmethod
    def cover_artist(person: Person) -> Credit:
        return Credit(person, COVER_ARTIST)

    @staticmethod
    def design(person: Person) -> Credit:
        return Credit(person, DESIGN)

    @staticmethod
    def editor(person: Person) -> Credit:
        return Credit(person, EDITOR)

    @staticmethod
    def foreword(person: Person) -> Credit:
        return Credit(person, FOREWORD)

    @staticmethod
    def inker(person: Person) -> Credit:
        return Credit(person, INKER)

    @staticmethod
    def letterer(person: Person) -> Credit:
        return Credit(person, LETTERER)

    @staticmethod
    def other(person: Person) -> Credit:
        return Credit(person, OTHER)

    @staticmethod
    def penciller(person: Person) -> Credit:
        return Credit(person, PENCILLER)

    @staticmethod
    def scenery(person: Person) -> Credit:
        return Credit(person, SCENERY)

    @staticmethod
    def storyboard(person: Person) -> Credit:
        return Credit(person, STORYBOARD)

    @staticmethod
    def translator(person: Person) -> Credit:
        return Credit(person, TRANSLATOR)

    @staticmethod
    def writer(person: Person) -> Credit:
        return Credit(person, WRITER)

    def primary(self) -> Credit:
        return Credit(self.person, self.role, True)

    def not_primary(self) -> Credit:
        return Credit(self.person, self.role, False)
