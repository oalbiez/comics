# -*- coding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass


@dataclass(frozen=True)
class Page:
    type: str | None
    bookmark: str | None
    double_page: bool | None
    image: int | None
    image_size: str | None
    image_height: str | None
    image_width: str | None
