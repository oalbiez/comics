# -*- coding: utf-8 -*-
from __future__ import annotations

from collections.abc import Callable, Iterable

from bs4 import BeautifulSoup  # type: ignore

from comics.model.metadata import (
    Credit,
    Data,
    Genre,
    Issue,
    Metadata,
    Person,
    PublicationDate,
    Publisher,
    Series,
    Title,
)
from comics.scraper.metadata import Criteria, ScrapedItem, ScraperDriver
from comics.utils.url import URL


class BdgestDriver(ScraperDriver):
    def collect_items(self, criteria: Criteria) -> list[ScrapedItem]:
        pass

    def metadata_for(self, url: URL) -> Metadata:
        pass


def sitemap_urls() -> Iterable[URL]:
    increment = 10_000
    lower = 1
    upper = 10_000
    while True:
        yield URL(f"https://www.bedetheque.com/albums_{lower}_{upper}_map.xml")
        lower += increment
        upper += increment


def extract_album_URL(content: str) -> Iterable[URL]:
    # pylint: disable=invalid-name
    xml = BeautifulSoup(content, "xml")
    for item in xml.find_all("xhtml:link"):
        yield URL(item.get("href"))


def text_of(elmt) -> str | None:
    if elmt:
        text = elmt.string
        if text:
            return str(text).strip()
        return str(" ".join(elmt.stripped_strings))
    return None


def _is_cover_url(url: URL) -> bool:
    return "/media/Couvertures/" in url.value


def _find_covers(content) -> Iterable[URL]:
    covers = content.find("div", id="album-slider")
    return filter(
        _is_cover_url, (URL(img.get("src")) for img in covers.find_all("img"))
    )


def _date(value: str) -> PublicationDate:
    parts: list[int] = list(map(int, value.split("/")))
    if len(parts) == 1:
        return PublicationDate(year=parts[0])
    if len(parts) == 2:
        return PublicationDate(year=parts[1], month=parts[0])
    if len(parts) == 3:
        return PublicationDate(year=parts[2], month=parts[1], day=parts[0])
    return PublicationDate()


_DETAILS_FIELDS: dict[str, Callable[[str], list[Data]]] = {
    "Série": lambda v: [Series(v)],
    "Titre": lambda v: [Title(v)],
    "Editeur": lambda v: [Publisher(v)],
    "Dépot légal": lambda v: [_date(v)],
    "Style": lambda v: [Genre(r.strip()) for r in v.split(",")],
    "Tome": lambda v: [Issue(v)],
    # Credits
    "Autres": lambda v: [Credit.other(Person(v))],
    "Couleurs": lambda v: [Credit.colorist(Person(v))],
    "Couverture": lambda v: [Credit.cover_artist(Person(v))],
    "Décors": lambda v: [Credit.scenery(Person(v))],
    "Design": lambda v: [Credit.design(Person(v))],
    "Dessin": lambda v: [Credit.penciller(Person(v))],
    "Encrage": lambda v: [Credit.inker(Person(v))],
    "Lettrage": lambda v: [Credit.letterer(Person(v))],
    "Préface": lambda v: [Credit.foreword(Person(v))],
    "Scénario": lambda v: [Credit.writer(Person(v))],
    "Storyboard": lambda v: [Credit.storyboard(Person(v))],
    "Traduction": lambda v: [Credit.translator(Person(v))],
    # <x> éditions
    # Adapté de -> Ajout d'un tag ?
    # Collection -> ??? | C'est la collection de l'editeur
    # Cycle -> ??? | C'est un numéro au sein de la série
    # Format -> Format
    # Identifiant -> URL to source
    # ISBN -> ISBN
    # Note -> Rate
    # Origine -> Country ?
    # Planches -> page count ?
    # Résumé -> Plot.Summary
}


def _parse_details(details) -> Metadata:
    actions = []
    for item in details.find_all("li"):
        text = text_of(item)
        if text is None:
            continue
        parts = text.split(":", maxsplit=1)
        if len(parts) != 2:
            continue

        key = parts[0].strip()
        value = parts[1].strip()
        if key in _DETAILS_FIELDS:
            actions.extend(_DETAILS_FIELDS[key](value))
    return Metadata(actions)


def serie_of(data: str) -> Iterable[URL]:
    soup = BeautifulSoup(data, "html.parser")
    for item in soup.find_all("a"):
        url = item.get("href")
        if url.startswith("https://m.bedetheque.com/serie-"):
            yield URL(url)


def album_to_metadata(data: str) -> Metadata:
    soup = BeautifulSoup(data, "html.parser")
    content = soup.find("div", attrs={"data-role": "content"})
    for cover in _find_covers(content):
        print(cover.value)
    details = content.find("ul", id="album-detail")
    return _parse_details(details)
