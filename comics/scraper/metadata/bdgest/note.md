?RechIdSerie=&RechSerie=&RechTitre=arthur


https://m.bedetheque.com/album?RechIdSerie=&RechSerie=&RechTitre=arthur

https://m.bedetheque.com/album?RechIdSerie=12&RechSerie=Thorgal&RechTitre=&RechIdAuteur=&RechAuteur=&RechEditeur=&RechCollection=&RechOrigine=&RechDL=&RechISBN=&RechEO=0



    357 794 Albums
    40 049  Revues
    62 559  Séries
    42 550  Auteurs


EDITOR = Role("Editor")


    "Editeur": "Publisher",
    "Language": "LanguageISO",
    "Tome": "Volume",
    "Planches": "PageCount",
    "publisher": "Publisher",
    "score": "Notes",

# <x> éditions
# Achev. impr. -> X
# Adapté de -> Ajout d'un tag ?
# Autre image -> X
# Autres info -> X
# Autres -> Credits;
# Collection -> ??? | C'est la collection de l'editeur
# Couleurs -> COLORIST
# Couverture -> COVER_ARTIST
# Créé le -> X
# Cycle -> ??? | C'est un numéro au sein de la série
# Décors -> Credits;
# Dépot légal -> Date
# Design -> Credits;
# Dessin -> PENCILLER
# Editeur -> Publisher
# Encrage -> INKER
# Estimation -> X
# Format -> Format
# Identifiant -> URL to source
# Info édition -> X
# ISBN -> ISBN
# Lettrage -> LETTERER
# Note -> Rate
# Origine -> Country ?
# PDF -> X
# Planches -> page count ?
# Préface -> Credits;
# Résumé -> Plot.Summary
# Scénario -> WRITER
# Série -> Series
# Storyboard -> Credits;
# Style -> Genre
# Titre -> Title
# Tome -> Issue
# Traduction -> TRANSLATOR