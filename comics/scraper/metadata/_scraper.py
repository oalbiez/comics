# -*- coding: utf-8 -*-
from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass, field

from comics.model.metadata import Metadata
from comics.utils.url import URL


class Getter(ABC):
    @abstractmethod
    def get(self, url: URL) -> str | None:
        """Get content of url"""


@dataclass(frozen=True)
class Criteria:
    series: str = ""
    issue: str = ""
    title: str = ""
    language: str = ""
    publisher: str = ""
    year: str = ""

    @staticmethod
    def undefined() -> Criteria:
        return Criteria()

    @staticmethod
    def on_series(value: str) -> Criteria:
        return Criteria(series=value)

    def with_title(self, value: str) -> Criteria:
        return Criteria(
            self.series,
            self.issue,
            value,
            self.language,
            self.publisher,
            self.year,
        )


@dataclass(frozen=True)
class ScrapedItem:
    # pylint: disable=too-many-instance-attributes
    url: URL
    title: str | None = None
    series: str | None = None
    issue: str | None = None
    publisher: str | None = None
    year: int | None = None
    language: str | None = None
    cover_hash: set[str] = field(default_factory=set)


class ScraperDriver(ABC):
    @abstractmethod
    def collect_items(self, criteria: Criteria) -> list[ScrapedItem]:
        """Collect items"""

    @abstractmethod
    def metadata_for(self, url: URL) -> Metadata:
        """Scrap metadata"""


class MetadataScraper:
    def __init__(self, getter: Getter, driver: ScraperDriver):
        self.getter = getter
        self.driver = driver
